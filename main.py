from fractions import Fraction

def find_pivot(ma, i):
    if ma[i][i] != 0:
        return i
    return find_row_to_swap(ma, i)

def reduce_row(row, pivot_row, yo, column):
    h = column
    while h < len(row):
        row[h] = row[h] - (yo*pivot_row[h])
        h += 1
    return row

def reduce_pivot_row(row, re):
    k = 0
    while k < len(row):
        row[k] = re * row[k]
        k += 1
    return row

def get_value_to_reduce_row(p, e_toreduce):
    return Fraction(p, e_toreduce)

def get_value_to_reduce__pivot_row(e: Fraction):
    return Fraction(e.denominator, e.numerator)


def debug(m):
    i = 1
    for row in m:
        row_format = ("{:>5}" * (len(row)-1)) + " | {}"
        print(f"E{i}:" + row_format.format(*[str(c) for c in row]))
        i += 1
    print()
    #input("->")

def find_row_to_swap(m, i):
    j = i
    while j < len(m):
        if m[j][i] != 0:
            return j
        j += 1
    return None

def reduce(m):
    i = 0
    debug(m)

    while i < len(m):
        pivot = find_pivot(m, i)
        if pivot is None:
            print(f"not found pivot for row {i}", m)
            debug(m)
            exit(-1)
        elif i != pivot:
                m[i], m[pivot] = m[pivot], m[i]
                print(f"E{i+1}<-->E{pivot+1}")
                pivot = i
                debug(m)

        print(f"\nPivot in  [{i},{pivot}]")
        #debug(m)

        j = 0

        while j < len(m):
            if i != j:  # not pivot row
                a = get_value_to_reduce_row(m[j][pivot], m[i][pivot])
                print(f"E{j+1}=E{j+1}-({a}*E{i+1})")
                m[j] = reduce_row(m[j], m[i], a, pivot)
                debug(m)
            j += 1

        s = get_value_to_reduce__pivot_row(m[i][pivot])
        m[i] = reduce_pivot_row(m[i], s)
        print(f"E{i+1}={s}*E{i+1}")
        debug(m)

        i += 1

    print(find_pivot(m, 0))


if __name__ == '__main__':
    """reduce([
        [Fraction(1, 2), Fraction(3, 2), Fraction(1, 1), Fraction(1, 1)],
        [Fraction(2, 1), Fraction(-3, 1), Fraction(1, 1), Fraction(1, 1)],
        [Fraction(1, 1), Fraction(6, 1), Fraction(1, 1), Fraction(5, 1)],
    ])"""
    """reduce([
        [Fraction(2, 1), Fraction(1, 2), Fraction(1, 3), Fraction(1, 1), Fraction(1, 1)],
        [Fraction(1, 1), Fraction(-1, 1), Fraction(2, 1), Fraction(1, 1), Fraction(1, 1)],
        [Fraction(3, 1), Fraction(2, 1), Fraction(-1, 1), Fraction(2, 1), Fraction(1, 1)],
    ])"""
    reduce([
        [Fraction(0, 1), Fraction(-1, 1), Fraction(0, 1), Fraction(1, 1), Fraction(1, 1)],
        [Fraction(2, 1), Fraction(1, 1), Fraction(-1, 1), Fraction(2, 1), Fraction(3, 1)],
        [Fraction(3, 1), Fraction(0, 1), Fraction(1, 1), Fraction(1, 1), Fraction(2, 1)],
    ])

